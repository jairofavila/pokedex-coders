package com.jairoavila.local.datasource

import com.jairoavila.common.model.Pokemon
import kotlinx.coroutines.flow.Flow

/**
 * Created by Jairo Avila on 15/06/20.
 */
interface LocalDataSource {

    suspend fun insertPokemonList(pokemonList: List<Pokemon>)

    fun getPokemonList(page_: Int): Flow<List<Pokemon>>
}
