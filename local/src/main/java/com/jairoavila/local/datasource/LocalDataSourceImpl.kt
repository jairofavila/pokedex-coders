package com.jairoavila.local.datasource

import com.jairoavila.common.model.Pokemon
import com.jairoavila.local.database.dao.PokemonDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class LocalDataSourceImpl(
    private val pokemonDao: PokemonDao
) : LocalDataSource {

    override suspend fun insertPokemonList(pokemonList: List<Pokemon>) =
        withContext(Dispatchers.IO) {
            pokemonDao.insertPokemonList(pokemonList)
        }

    override fun getPokemonList(page_: Int): Flow<List<Pokemon>> = pokemonDao.getPokemonList(page_)
}
