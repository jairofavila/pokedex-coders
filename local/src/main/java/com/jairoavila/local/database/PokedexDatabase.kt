package com.jairoavila.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.jairoavila.common.model.Pokemon
import com.jairoavila.local.database.dao.PokemonDao

/**
 * Created by Jairo Avila on 14/06/20.
 */

@Database(entities = [Pokemon::class], version = 1)
abstract class PokedexDatabase : RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
}
