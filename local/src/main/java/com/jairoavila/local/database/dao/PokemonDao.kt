package com.jairoavila.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jairoavila.common.model.Pokemon
import kotlinx.coroutines.flow.Flow

/**
 * Created by Jairo Avila on 22/06/20.
 */

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemonList(pokemonList: List<Pokemon>)

    @Query("SELECT * FROM Pokemon WHERE page = :page_")
    fun getPokemonList(page_: Int): Flow<List<Pokemon>>
}
