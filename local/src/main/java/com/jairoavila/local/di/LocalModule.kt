package com.jairoavila.local.di

import android.app.Application
import androidx.room.Room
import com.jairoavila.local.database.PokedexDatabase
import com.jairoavila.local.database.dao.PokemonDao
import com.jairoavila.local.datasource.LocalDataSource
import com.jairoavila.local.datasource.LocalDataSourceImpl
import com.jairoavila.local.preference.PreferenceHelper
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Created by Jairo Avila on 14/06/20.
 */

const val DATABASE_NAME = "pokedex_db"

val localModule = module {
    single { providePreferenceHelper(androidApplication()) }
    single { provideRoom(androidApplication()) }
    single { providePokemonDao(get()) }
    single<LocalDataSource> { LocalDataSourceImpl(get()) }
}

fun providePreferenceHelper(androidApplication: Application): PreferenceHelper =
    PreferenceHelper(androidApplication)

fun provideRoom(application: Application): PokedexDatabase {
    return Room.databaseBuilder(
        application.applicationContext,
        PokedexDatabase::class.java,
        DATABASE_NAME
    ).build()
}

fun providePokemonDao(pokedexDatabase: PokedexDatabase): PokemonDao = pokedexDatabase.pokemonDao()
