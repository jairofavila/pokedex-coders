package com.jairoavila.common.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Jairo Avila on 22/06/20.
 */

@Entity
data class Pokemon(
    var page: Int = 0,
    @PrimaryKey val name: String,
    val url: String
)
