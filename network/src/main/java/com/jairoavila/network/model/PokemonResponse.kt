package com.jairoavila.network.model

import com.jairoavila.common.model.Pokemon

/**
 * Created by Jairo Avila on 22/06/20.
 */

data class PokemonResponse(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<Pokemon>
)
