package com.jairoavila.network.datasource

import com.jairoavila.network.model.PokemonResponse
import com.jairoavila.network.utils.Resource

/**
 * Created by Jairo Avila on 15/06/20.
 */
interface RemoteDataSource {

    suspend fun getPokemonListRemote(): Resource<PokemonResponse>
}
