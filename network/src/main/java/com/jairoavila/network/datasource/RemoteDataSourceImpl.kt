package com.jairoavila.network.datasource

import com.jairoavila.network.PokeApi
import com.jairoavila.network.model.PokemonResponse
import com.jairoavila.network.utils.Resource
import com.jairoavila.network.utils.ResponseHandler
import kotlin.Exception

class RemoteDataSourceImpl(
    private val pokeApi: PokeApi,
    private val responseHandler: ResponseHandler
) : RemoteDataSource {

    override suspend fun getPokemonListRemote(): Resource<PokemonResponse> =
        try {
            val response = pokeApi.fetchPokemonList(20, 10)
            responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
}
