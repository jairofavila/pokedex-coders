package com.jairoavila.network

import com.jairoavila.network.model.PokemonResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Jairo Avila on 14/06/20.
 */
interface PokeApi {

    @GET("pokemon")
    suspend fun fetchPokemonList(
        @Query("limit") limit: Int = 20,
        @Query("offset") offset: Int = 0
    ): PokemonResponse
}
