package com.jairoavila.network.di

import com.jairoavila.network.PokeApi
import com.jairoavila.network.datasource.RemoteDataSource
import com.jairoavila.network.datasource.RemoteDataSourceImpl
import com.jairoavila.network.utils.ResponseHandler
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Jairo Avila on 14/06/20.
 */

const val API_URL = "https://pokeapi.co/api/v2/"

val networkModule = module {
    factory { provideLoggingInterceptor() }
    factory { provideOkHttpClient(get()) }
    single { provideRetrofit(get()) }
    factory { providePokeApi(get()) }
    factory { ResponseHandler() }

    single<RemoteDataSource> { RemoteDataSourceImpl(get(), get()) }
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
}

fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().apply {
        addInterceptor(interceptor)
    }.build()
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().apply {
        baseUrl(API_URL)
        client(okHttpClient)
        addConverterFactory(GsonConverterFactory.create())
    }.build()
}

fun providePokeApi(retrofit: Retrofit): PokeApi = retrofit.create(PokeApi::class.java)
