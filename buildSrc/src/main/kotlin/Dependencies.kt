object BuildPlugins {

    object Versions {
        const val kotlin_version = "1.3.72"
        const val gradle_version = "4.0.0"
        const val navigation_version = "2.3.0-alpha05"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.gradle_version}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin_version}"
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin_version}"
    const val safeArgsPlugin = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation_version}"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExtensions = "kotlin-android-extensions"
    const val kotlinSafeArgs = "androidx.navigation.safeargs.kotlin"
}

object AndroidSdk {
    const val compileSdk = 30
    const val buildTools = "29.0.3"
    const val minSdk= 21
    const val targetSdk = 30
}

object SupportLibraries {
    private object Versions {
        const val appcompat_version = "1.1.0"
        const val corektx_version = "1.2.0"
        const val legacy_version = "1.0.0"
    }

    const val appCompat = "androidx.appcompat:appcompat:${Versions.appcompat_version}"
    const val ktxCore = "androidx.core:core-ktx:${Versions.corektx_version}"
    const val legacy =  "androidx.legacy:legacy-support-v4:${Versions.legacy_version}"
}

object UILibraries {
    private object Versions {
        const val constraintLayout_version = "2.0.0-beta4"
        const val recyclerview_version = "1.1.0"
        const val cardview_version = "1.0.0"
        const val material_version = "1.2.0-alpha06"
    }

    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout_version}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerview_version}"
    const val cardView = "androidx.cardview:cardview:${Versions.cardview_version}"
    const val materialDesign = "com.google.android.material:material:${Versions.material_version}"
}

object ArchComponentsLibraries {
    private object Versions {
        const val lifecycle_version = "2.2.0"
        const val navigation_version = "2.3.0-alpha05"
        const val paging_version = "2.1.2"
        const val room_version = "2.2.5"
    }

    const val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle_version}"
    const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle_version}"
    const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle_version}"
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation_version}"
    const val navigationUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigation_version}"
    const val paging =  "androidx.paging:paging-runtime:${Versions.paging_version}"
    const val roomRuntime =  "androidx.room:room-ktx:${Versions.room_version}"
    const val roomCompiler =  "androidx.room:room-compiler:${Versions.room_version}"
}

object NetworkLibraries {
    private object Versions {
        const val retrofit_version = "2.9.0"
        const val moshi_version = "1.6.0"
        const val moshi_converter_version = "2.3.0"
        const val okhttp_version = "4.7.2"
    }

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit_version}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp_version}"
    const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp_version}"
    const val gsonConverter ="com.squareup.retrofit2:converter-gson:${Versions.retrofit_version}"
    const val moshi = "com.squareup.moshi:moshi:${Versions.moshi_version}"
    const val moshiKotlin = "com.squareup.moshi:moshi-kotlin:${Versions.moshi_version}"
    const val moshiCodegen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi_version}"
    const val moshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.moshi_converter_version}"
}

object DILibraries {
    private object Versions {
        const val koin_version = "2.1.5"
    }

    const val koin = "org.koin:koin-android:${Versions.koin_version}"
    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin_version}"
    const val koinViewmodel = "org.koin:koin-androidx-viewmodel:${Versions.koin_version}"
    const val koinFragment = "org.koin:koin-androidx-fragment:${Versions.koin_version}"

}

object UtilsLibraries {
    private object Versions {
        const val coil_version = "0.11.0"
        const val leak_canary = "2.3"
    }

    const val coil = "io.coil-kt:coil:${Versions.coil_version}"
    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leak_canary}"
}

object AnalysisCodeLibraries {
    object AnalysisCodeVersions {
        const val ktlint_gradle_version = "8.2.0"
        const val ktlint_version = "0.36.0"
    }

    const val ktlinGradle = "org.jlleitschuh.gradle.ktlint"
    const val ktlint = "org.jlleitschuh.gradle.ktlint"
}

object TestLibraries {
    private object Versions {
        const val junit4_version = "4.13"
        const val extJunit_version = "1.1.1"
        const val espresso_version = "3.2.0"
    }

    const val junit4 = "junit:junit:${Versions.junit4_version}"
    const val extJunit = "androidx.test.ext:junit:${Versions.extJunit_version}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso_version}"
}