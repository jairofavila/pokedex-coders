package com.jairoavila.data.repository

import com.jairoavila.common.model.Pokemon
import com.jairoavila.data.utils.NetworkBoundResource
import com.jairoavila.local.datasource.LocalDataSource
import com.jairoavila.network.datasource.RemoteDataSource
import com.jairoavila.network.model.PokemonResponse
import com.jairoavila.network.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
class PokedexRepositoryImpl(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : PokedexRepository {

    override fun getPokemonList(): Flow<Resource<List<Pokemon>>> {
        return object : NetworkBoundResource<List<Pokemon>, PokemonResponse>() {
            override fun processResponse(response: PokemonResponse): List<Pokemon> {
                return response.results
            }

            override suspend fun saveNetworkResult(items: List<Pokemon>) {
                return localDataSource.insertPokemonList(items)
            }

            override fun shouldFetch(data: List<Pokemon>?): Boolean {
                return true
            }

            override fun loadFromDb(): Flow<List<Pokemon>> {
                return localDataSource.getPokemonList(0)
            }

            override suspend fun fetchFromNetwork(): Resource<PokemonResponse> {
                return remoteDataSource.getPokemonListRemote()
            }
        }.asFlow()
    }
}
