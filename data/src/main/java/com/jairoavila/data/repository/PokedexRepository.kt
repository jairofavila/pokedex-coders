package com.jairoavila.data.repository

import com.jairoavila.common.model.Pokemon
import com.jairoavila.network.utils.Resource
import kotlinx.coroutines.flow.Flow

/**
 * Created by Jairo Avila on 15/06/20.
 */
interface PokedexRepository {

    fun getPokemonList(): Flow<Resource<List<Pokemon>>>
}
