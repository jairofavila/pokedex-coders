package com.jairoavila.data.di

import com.jairoavila.data.repository.PokedexRepository
import com.jairoavila.data.repository.PokedexRepositoryImpl
import org.koin.dsl.module

/**
 * Created by Jairo Avila on 15/06/20.
 */

val repositoryModule = module {
    single<PokedexRepository> { PokedexRepositoryImpl(get(), get()) }
}
