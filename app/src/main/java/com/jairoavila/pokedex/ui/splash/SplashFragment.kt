package com.jairoavila.pokedex.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jairoavila.pokedex.R
import com.jairoavila.pokedex.databinding.SplashFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Jairo Avila on 31/05/20.
 */

class SplashFragment : Fragment() {

    private var _binding: SplashFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewmodel: SplashViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SplashFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpStartAnimation()
        setUpListeners()
        onViewmodelObserver()
        startDashboard()
    }

    private fun setUpListeners() {
        binding.mlSplash.setTransitionListener(object : MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {
                // Not used
            }

            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
                // Not used
            }

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
                // Not used
            }

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) = setUpStartAnimation()
        })
    }

    private fun setUpStartAnimation() {
        binding.mlSplash.transitionToStart()
        binding.mlSplash.transitionToEnd()
    }

    private fun onViewmodelObserver() {
    }

    private fun startDashboard() {
        Handler(Looper.myLooper()!!).postDelayed({
            findNavController().navigate(R.id.action_splashFragment_to_dashboardFragment)
        }, 5000)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
