package com.jairoavila.pokedex.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jairoavila.pokedex.databinding.ActivityPokedexBinding

/**
 * Created by Jairo Avila on 31/05/20.
 */

class PokedexActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityPokedexBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
