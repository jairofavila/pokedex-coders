package com.jairoavila.pokedex.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.jairoavila.common.model.Pokemon
import com.jairoavila.data.repository.PokedexRepository
import com.jairoavila.network.utils.Resource

class DashboardViewModel(
    private val repository: PokedexRepository
) : ViewModel() {

    val getPokemon: LiveData<Resource<List<Pokemon>>> = repository.getPokemonList().asLiveData()
}
