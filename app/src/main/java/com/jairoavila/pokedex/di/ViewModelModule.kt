package com.jairoavila.pokedex.di

import com.jairoavila.pokedex.ui.dashboard.DashboardViewModel
import com.jairoavila.pokedex.ui.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Jairo Avila on 31/05/20.
 */

val viewmodelModule = module {
    viewModel { SplashViewModel() }
    viewModel { DashboardViewModel(get()) }
}
