package com.jairoavila.pokedex

import android.app.Application
import com.jairoavila.data.di.repositoryModule
import com.jairoavila.local.di.localModule
import com.jairoavila.network.di.networkModule
import com.jairoavila.pokedex.di.viewmodelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Jairo Avila on 31/05/20.
 */

class PokedexApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@PokedexApplication)
            modules(
                arrayListOf(
                    viewmodelModule,
                    repositoryModule,
                    networkModule,
                    localModule
                )
            )
        }
    }
}
