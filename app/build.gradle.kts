plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.kotlinAndroidExtensions)
    kotlin("kapt")
}

android {
    compileSdkVersion(AndroidSdk.compileSdk)
    buildToolsVersion(AndroidSdk.buildTools)

    defaultConfig {
        applicationId = "com.jairoavila.pokedex"
        minSdkVersion(AndroidSdk.minSdk)
        targetSdkVersion(AndroidSdk.targetSdk)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    // Support Libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(BuildPlugins.kotlinStdLib)
    implementation(SupportLibraries.appCompat)
    implementation(SupportLibraries.ktxCore)
    implementation(SupportLibraries.legacy)

    // UI Libraries
    implementation(UILibraries.constraintLayout)
    implementation(UILibraries.recyclerView)
    implementation(UILibraries.cardView)
    implementation(UILibraries.materialDesign)

    // Architecture Components Libraries
    implementation(ArchComponentsLibraries.viewmodel)
    implementation(ArchComponentsLibraries.lifecycleExtensions)
    implementation(ArchComponentsLibraries.livedata)
    implementation(ArchComponentsLibraries.navigationFragment)
    implementation(ArchComponentsLibraries.navigationUI)
    implementation(ArchComponentsLibraries.paging)

    // Dependency Injection Library
    implementation(DILibraries.koinViewmodel)
    implementation(DILibraries.koinFragment)
    implementation(DILibraries.koin)
    implementation(DILibraries.koinScope)

    // Utils Libraries
    implementation(UtilsLibraries.coil)
    implementation(UtilsLibraries.leakCanary)

    // Test Libraries
    testImplementation(TestLibraries.junit4)
    testImplementation(TestLibraries.extJunit)
    androidTestImplementation(TestLibraries.espresso)

    // Modules
    implementation(project(":data"))
    implementation(project(":local"))
    implementation(project(":network"))
    implementation(project(":common"))
}
