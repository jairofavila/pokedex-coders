// Top-level build file where you can add configuration options common to all sub-projects/modules.

plugins {
    id(AnalysisCodeLibraries.ktlinGradle) version AnalysisCodeLibraries.AnalysisCodeVersions.ktlint_gradle_version
}

buildscript {
    repositories {
        google()
        jcenter()
    }

    dependencies {
        classpath(BuildPlugins.androidGradlePlugin)
        classpath(BuildPlugins.kotlinGradlePlugin)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

subprojects {
    apply(plugin = AnalysisCodeLibraries.ktlint)

    ktlint {
        version.set(AnalysisCodeLibraries.AnalysisCodeVersions.ktlint_version)
        debug.set(true)
        verbose.set(true)
        android.set(false)
        outputToConsole.set(true)
        reporters.set(
            setOf(
                org.jlleitschuh.gradle.ktlint.reporter.ReporterType.PLAIN,
                org.jlleitschuh.gradle.ktlint.reporter.ReporterType.CHECKSTYLE
            )
        )
        ignoreFailures.set(true)
        kotlinScriptAdditionalPaths {
            include(fileTree("scripts/"))
        }
        filter {
            exclude("**/generated/**")
            include("**/kotlin/**")
        }
    }
}

tasks.register("clean").configure {
    delete("build")
}
